# Assignment KLM
The assignment was very enjoyable. It is my first time to write a data structure in python dataclasses - it was a nice experience! I think I am content with my solution, so I am looking forward to hearing your feedback.

I took approximately 3 hours for the assignment. Yet, I already did some drawing and thinking during last week. I first wrote the tests that were following the requirements of the assignment. After that I wrote the data model and services logic until all tests passed.

### File/Folder structure
```
src/
  models.py         # Data structure in python dataclasses;
  services.py       # Business logic;
tests/
  fixtures.py       # Some test data used in the pytest tests;
  test_all.py       # All integration tests;
requirements.txt    # Dependencies;
```

### Notes / Doubts
- In a real live I would introduce the concept of `Airport` to be the `location` of a `FlightEvent`. As it did not matter for this assignment, I left `location` a `string`;
- The filtering logic is build-in python logic. A limitation of this is that it cannot handle huge amounts of data. To solve that, I would use a proper ORM and leave the filter logic in the database;
- The method `add_booking` is business logic in the `models`. I left it that, as I wanted leave `_bookings` a private property;


### Run tests
To run the integration tests, you can run:
```
python -m pytest -v
```
