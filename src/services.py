from datetime import datetime
from typing import List

from .models import Booking
from .models import Flight


def filter_flights_from(flights: List[Flight], timestamp: datetime) -> List[Flight]:
    """ Returns a selection of flights in which the departure time is greater than a give timestamp """
    return list(filter(lambda flight: flight.departure.timestamp > timestamp, flights))


def filter_bookings_by_number_of_flights(bookings: List[Booking], number_of_flights: int) -> List[Booking]:
    """ Returns a selection of bookings that contain a given number of flights """
    return list(filter(lambda booking: len(booking.flights) == number_of_flights, bookings))
