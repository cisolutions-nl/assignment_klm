from dataclasses import dataclass, field
from datetime import datetime
import itertools
import operator
from typing import List, Optional


@dataclass
class FlightEvent:
    """ A departure or an arrival """
    location: str
    timestamp: Optional[datetime] = field(default=None)  # Departure or arrival time can be unknown;


@dataclass(order=True)
class Flight:
    """ Defines one trip of an airplane from a departure to an arrival location """
    sort_index: int = field(init=False, repr=False, compare=True)

    departure: FlightEvent
    arrival: FlightEvent

    def __post_init__(self):
        self.sort_index = int(self.departure.timestamp.timestamp())


@dataclass
class Booking:
    """ Defines a person's trip from location A to location B
    (NB: can contain multiple flights; hence, layovers); """
    flights: List[Flight] = field(repr=False, default_factory=list)

    def __repr__(self):
        return f'Booking({self.departure_location or "unknown"} - {self.arrival_location or "unknown"})'

    def __eq__(self, other):
        return self.flights == other.flights

    @property
    def sorted_flights(self):
        return sorted(
            self.flights,
            key=operator.attrgetter('sort_index')
        )

    @property
    def departure_location(self) -> Optional[str]:
        """ Returns the departure location of this booking """
        try:
            return self.sorted_flights[0].departure.location
        except IndexError:
            return None

    @property
    def arrival_location(self) -> Optional[str]:
        """ Returns the departure location of this booking """
        try:
            return self.sorted_flights[-1].arrival.location
        except IndexError:
            return None


@dataclass
class Passenger:
    """ Data class that defines a person that has bookings; """
    name: str
    _bookings: List[Booking] = field(default_factory=list, init=False, repr=False)

    @property
    def bookings(self) -> List[Booking]:
        """ Returns a list of all the bookings of this passenger; """
        return self._bookings

    @property
    def flights(self) -> List[Flight]:
        """ Returns all flights (ordered by departure timestamp) of this passenger, irrespective of bookings """
        return sorted(
            itertools.chain(*[booking.flights for booking in self.bookings]),
            key=operator.attrgetter('sort_index')
        )

    def add_booking(self, booking: Booking) -> None:
        """ Adds one booking to this passenger """
        if booking in self.bookings:
            raise ValueError('Passenger already has this booking')

        self._bookings.append(booking)
