from datetime import datetime, timedelta

import pytest

from src.models import Booking, Passenger
from src.services import filter_flights_from, filter_bookings_by_number_of_flights

from .fixtures import flights_list

now = datetime.now()


def test_add_booking():
    # Generate the necessary data for this test;
    passenger = Passenger(name='Sjaak')

    # Test if initial number of bookings and flights is zero;
    assert len(passenger.bookings) == 0
    assert len(passenger.flights) == 0

    # Create and add a booking to the passenger;
    booking = Booking(flights=flights_list[1])
    passenger.add_booking(booking)

    # Test the number of bookings and flights again;
    assert len(passenger.bookings) == 1
    assert len(passenger.flights) == 2


def test_add_same_booking():
    # Generate the necessary data for this test;
    passenger = Passenger(name='Linda')
    booking = Booking(flights=flights_list[0])
    passenger.add_booking(booking)

    # Assertions;
    assert len(passenger.bookings) == 1

    with pytest.raises(ValueError, match=r"Passenger already has this booking"):
        passenger.add_booking(booking)


def test_booking_departure_and_arrival():
    booking = Booking()
    assert booking.departure_location is None
    assert booking.arrival_location is None

    booking.flights = flights_list[0]

    assert booking.departure_location == 'AAL'
    assert booking.arrival_location == 'AMS'

    booking = Booking()
    booking.flights = flights_list[1]
    assert booking.departure_location == 'GVA'
    assert booking.arrival_location == 'WAW'


def test_select_flight_on_departure_time():
    # Generate the necessary data for this test;
    passenger = Passenger(name='Cindy')
    for flights in flights_list:
        passenger.add_booking(Booking(flights=flights))

    # Select the flights;
    from_timestamp = now + timedelta(hours=33)
    filtered_flights = filter_flights_from(passenger.flights, from_timestamp)

    # Assertions;
    for selected_flight in filtered_flights:
        assert selected_flight.departure.timestamp > from_timestamp

    assert len(filtered_flights) < len(passenger.flights)


def test_select_booking_on_number_of_flights():
    # Generate the necessary data for this test;
    bookings = [Booking(flights=flights) for flights in flights_list]

    # Filter the bookings;
    filtered_bookings_2 = filter_bookings_by_number_of_flights(bookings=bookings, number_of_flights=2)
    filtered_bookings_3 = filter_bookings_by_number_of_flights(bookings=bookings, number_of_flights=3)

    # Assertions;
    assert len(filtered_bookings_2) == 2
    assert len(filtered_bookings_3) == 1
