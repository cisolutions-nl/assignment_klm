from datetime import datetime, timedelta

from src.models import Flight
from src.models import FlightEvent

now = datetime.now()

flights_list = [
    # Alice's flights;
    [
        Flight(
            departure=FlightEvent(location='AAL', timestamp=now + timedelta(hours=24)),
            arrival=FlightEvent(location='AMS'),
        )
    ],
    # Bruce's flights;
    [
        Flight(
            departure=FlightEvent(location='GVA', timestamp=now + timedelta(hours=24)),
            arrival=FlightEvent(location='CDG'),
        ),
        Flight(
            departure=FlightEvent(location='CDG', timestamp=now + timedelta(hours=32)),
            arrival=FlightEvent(location='WAW'),
        )
    ],
    # Cindy's flights;
    [
        Flight(
            departure=FlightEvent(location='AMS', timestamp=now + timedelta(hours=48)),
            arrival=FlightEvent(location='LHR'),
        ),
        Flight(
            departure=FlightEvent(location='LHR', timestamp=now + timedelta(hours=58)),
            arrival=FlightEvent(location='JFK'),
        ),
        Flight(
            departure=FlightEvent(location='JFK', timestamp=now + timedelta(hours=63)),
            arrival=FlightEvent(location='ATL'),
        )
    ],
    # Derek's flights;
    [
        Flight(
            departure=FlightEvent(location='AMS', timestamp=now + timedelta(hours=32)),
            arrival=FlightEvent(location='ATL'),
        ),
    ],
    # Erica's flights;
    [
        Flight(
            departure=FlightEvent(location='ATL', timestamp=now + timedelta(hours=10)),
            arrival=FlightEvent(location='AMS'),
        ),
        Flight(
            departure=FlightEvent(location='AMS', timestamp=now + timedelta(hours=24)),
            arrival=FlightEvent(location='AAL'),
        ),
    ]
]
